<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Band;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $this->loadBands($manager);
        $manager->flush();
    }
    private function loadBands(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $band = new Band();
            $band->setCountry('Country ' . $i);
            $band->setStart(new \DateTime());
            if ($i % 2 === 0) {
                $band->setEnd(null);
            } else {
                $band->setEnd(new \DateTime());
            };
            $band->setName('Band name ' . $i);
            $manager->persist($band);
        }
        $manager->flush();
    }
}
