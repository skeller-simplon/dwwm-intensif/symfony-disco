<?php

namespace App\Controller;

use App\Entity\Band;
use App\Repository\BandRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\BandType;
use JMS\Serializer\SerializerInterface;

/**
 * @Route("/api/band")
 */
class BandController extends AbstractController
{
   /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route(methods="GET")
     */
    public function getAllBands(BandRepository $appliRepository)
    {
        return new JsonResponse(
            $this->serializer->serialize(
                $appliRepository->findAll(),
                'json'
            ),
            JsonResponse::HTTP_OK,
            [],
            true
        );
    }
    /**
     * @Route("/{band}", methods="GET")
     */
    public function getOneBand(Band $band =null){
        if ($band) {
            return new JsonResponse(
                $this->serializer->serialize($band, 'json'),
                JsonResponse::HTTP_OK,
                [],
                true
            );    
        }
        return $this->json('Band not found', JsonResponse::HTTP_NO_CONTENT);
    }
    /**
     * @Route(methods="POST")
     */
    public function createBand(ObjectManager $manager, Request $request)
    {
        $band = new Band();
        $form = $this->createForm(BandType::class, $band);
        $form->submit(
            json_decode(
                $request->getContent(),
                true
            )
        );
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($band);
            $manager->flush();
            return new JsonResponse(
                $this->serializer->serialize(
                    $band,
                    'json'
                ),
                201,
                [],
                true
            );
        }
        return $this->json($form->getErrors(true), 400);
    }

    /**
     * @Route("/{band}", methods="DELETE")
     */
    public function deleteBand(Band $band = null, ObjectManager $manager){
        if ($band) {
            $manager->remove($band);
            $manager->flush();
            return $this->json("Deleted with success", JsonResponse::HTTP_OK);
        }
        return $this->json('Band not found', JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/{band}", methods="PATCH")
     */
    public function patchBand(Band $band = null, ObjectManager $objectManager, Request $request){
        if ($band) {
            $form = $this->createForm(BandType::class, $band);
            $form->submit(
                json_decode($request->getContent(), true),
                false
            );
            if ($form->isSubmitted() && $form->isValid()) {
                $objectManager->flush();
                return new JsonResponse(
                    $this->serializer->serialize($band, 'json'),
                    JsonResponse::HTTP_OK,
                    [],
                    true
                );
            }
        }
        return $this->json('Band not found', JsonResponse::HTTP_NO_CONTENT);
    }
}
